package com.beltom.dao;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

import com.beltom.model.ShipmentDetail;

public class ShipmentDetailDao extends HibernateBaseDao {
	private static final Logger log = LogManager.getLogger(ShipmentDetailDao.class);
	
	/*
	 * 依出貨單號搜尋產品清單
	 */
	@SuppressWarnings("unchecked")
	public List<ShipmentDetail> queryShipmentDetailsByShipmentId(String shipmentId) {
		List<ShipmentDetail> shipmentDetails = new ArrayList<>();
		Transaction transaction = null;
		Session session = sessionFactory.getCurrentSession();
		
		try {
			transaction = session.beginTransaction();
			shipmentDetails = (List<ShipmentDetail>)session
					.createCriteria(ShipmentDetail.class)
					.add(Restrictions.eq("shipmentId", shipmentId))
					.list();
			transaction.commit();
		} catch(Exception e) {
			transaction.rollback();
			log.error("Exception : ", e);
		}
		
		return shipmentDetails;
	}
	
	/*
	 * 依出貨單號刪除產品清單
	 */
	public void deleteShipmentDetailsByShipmentId(String shipmentId) {
		Transaction transaction = null;
		Session session = sessionFactory.getCurrentSession();
		String hql = "delete from ShipmentDetail where shipmentId=:shipmentId";
		
		try {
			transaction = session.beginTransaction();
			session.createQuery(hql).setString("shipmentId", shipmentId).executeUpdate();
			
			transaction.commit();
		} catch(Exception e) {
			transaction.rollback();
			log.error("Exception : ", e);
		}
	}
	
	/*
	 * 下方為Product CRUD
	 */
	public void saveShipmentDetail(ShipmentDetail shipmentDetail) {
		super.save(shipmentDetail);
	}
	
	public ShipmentDetail getShipmentDetailById(Integer id) {
		return (ShipmentDetail)super.get(ShipmentDetail.class, id);
	}
	
	public void updateShipmentDetail(ShipmentDetail shipmentDetail) {
		super.update(shipmentDetail);
	}
	
	public void deleteShipmentDetail(ShipmentDetail shipmentDetail) {
		super.delete(shipmentDetail);
	}
}
