package com.beltom.dao;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

import com.beltom.model.Shipment;
import com.beltom.model.ShipmentHistory;

public class ShipmentHistoryDao extends HibernateBaseDao {
	private static final Logger log = LogManager.getLogger(ShipmentHistoryDao.class);
	
	/*
	 * 依照出貨單號搜尋出貨歷程
	 */
	@SuppressWarnings("unchecked")
	public List<ShipmentHistory> queryHistoryByShipmentId(String shipmentId) {
		List<ShipmentHistory> shipmentHistories = new ArrayList<>();
		Transaction transaction = null;
		Session session = sessionFactory.getCurrentSession();
		
		try {
			transaction = session.beginTransaction();
			shipmentHistories = (List<ShipmentHistory>)session
					.createCriteria(ShipmentHistory.class)
					.add(Restrictions.eq("shipmentId", shipmentId))
					.list();
			transaction.commit();
		} catch(Exception e) {
			transaction.rollback();
			log.error("Exception : ", e);
		}
		
		return shipmentHistories;
	}
	
	/*
	 * 下方為Product CRUD
	 */
	public void saveShipmentHistory(ShipmentHistory shipmentHistory) {
		super.save(shipmentHistory);
	}
	
	public ShipmentHistory getShipmentHistoryById(Integer id) {
		return (ShipmentHistory)super.get(Shipment.class, id);
	}
	
	public void updateShipmentHistory(ShipmentHistory shipmentHistory) {
		super.update(shipmentHistory);
	}
	
	public void deleteShipmentHistory(ShipmentHistory shipmentHistory) {
		super.delete(shipmentHistory);
	}
}
