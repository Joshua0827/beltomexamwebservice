package com.beltom.dao;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

import com.beltom.model.User;

public class UserDao extends HibernateBaseDao {
	private static final Logger log = LogManager.getLogger(UserDao.class);
	
	/*
	 * 利用使用者名稱取得使用者物件
	 */
	public User getUserByName(String userName) {
		User user = null;
		Transaction transaction = null;
		Session session = sessionFactory.getCurrentSession();
		
		try {
			transaction = session.beginTransaction();
			user = (User)session
					.createCriteria(User.class)
					.add(Restrictions.eq("userName", userName))
					.uniqueResult();
			transaction.commit();
		} catch(Exception e) {
			transaction.rollback();
			log.error("Exception : ", e);
		}
		
		return user;
	}
	
	/*
	 * 下方為User的CRUD
	 */
	public void saveUser(User user) {
		super.save(user);
	}
	
	public User getUserById(Integer id) {
		return (User)super.get(User.class, id);
	}
	
	public void updateUser(User user) {
		super.update(user);
	}
	
	public void deleteUser(User user) {
		super.delete(user);
	}
}
