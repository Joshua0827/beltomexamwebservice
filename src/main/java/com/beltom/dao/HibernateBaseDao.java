package com.beltom.dao;

import java.io.Serializable;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

/*
 * 抽象類別，預設基礎的CRUD
 * 靜態的SessionFactory，讓擴充類別可以自訂HQL與Criteria與其他更複雜的操作
 * Hibernate與底層DB的連線session由SessionFactory負責產生與管理生命週期
 * 透過工廠取得session並開啟transaction，接著執行DB操作
 * 最後程式只需要確實的呼叫transaction的commit()與rollback()即可
 */

public abstract class HibernateBaseDao {
	private static final Logger log = LogManager.getLogger(HibernateBaseDao.class);
	protected static final SessionFactory sessionFactory;
	
	static {
		try {
			Configuration configuration = new Configuration().configure("hibernate.cfg.xml");
			StandardServiceRegistryBuilder serviceRegistryBuilder = new StandardServiceRegistryBuilder();
			serviceRegistryBuilder.applySettings(configuration.getProperties());
			StandardServiceRegistry serviceRegistry = serviceRegistryBuilder.build();
			sessionFactory = configuration.buildSessionFactory(serviceRegistry);
		} catch(Throwable ex) {
			log.error("Exception : ", ex);
			throw new ExceptionInInitializerError(ex);
		}
	}
	
	protected void save(Object object) {
		Transaction transaction = null;
		Session session = sessionFactory.getCurrentSession();
		
		try {
			transaction = session.beginTransaction();
			session.save(object);
			transaction.commit();
		} catch(Exception e) {
			transaction.rollback();
			log.error("Exception : ", e);
		}
	}
	
	protected Object get(Class<?> clazz, Serializable id) {
		Object object = null;
		Transaction transaction = null;
		Session session = sessionFactory.getCurrentSession();
		
		try {
			transaction = session.beginTransaction();
			object = session.get(clazz, id);
			transaction.commit();
		} catch(Exception e) {
			transaction.rollback();
			log.error("Exception : ", e);
		}
		
		return object;
	}
	
	protected void update(Object object) {
		Transaction transaction = null;
		Session session = sessionFactory.getCurrentSession();
		
		try {
			transaction = session.beginTransaction();
			session.update(object);
			transaction.commit();
		} catch(Exception e) {
			transaction.rollback();
			log.error("Exception : ", e);
		} 
	}
	
	protected void delete(Object object) {
		Transaction transaction = null;
		Session session = sessionFactory.getCurrentSession();
		
		try {
			transaction = session.beginTransaction();
			session.delete(object);
			transaction.commit();
		} catch(Exception e) {
			transaction.rollback();
			log.error("Exception : ", e);
		} 
	}
}
