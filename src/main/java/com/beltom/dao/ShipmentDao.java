package com.beltom.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import com.beltom.model.Shipment;
import com.beltom.util.DateUtil;
import com.beltom.util.StringUtil;

public class ShipmentDao extends HibernateBaseDao {
	private static final Logger log = LogManager.getLogger(ShipmentDao.class);
	
	/*
	 * 取得最大出貨單號的出貨單
	 */
	public Shipment getMaxShipmentByHeader(String header) {
		Shipment shipment = null;
		Transaction transaction = null;
		Session session = sessionFactory.getCurrentSession();
		
		try {
			transaction = session.beginTransaction();
			shipment = (Shipment)session
					.createCriteria(Shipment.class)
					.add(Restrictions.ilike("shipmentId", header, MatchMode.ANYWHERE))
					.addOrder(Order.desc("shipmentId"))
					.setMaxResults(1)
					.uniqueResult();
			transaction.commit();
		} catch(Exception e) {
			transaction.rollback();
			log.error("Exception : ", e);
		}
		
		return shipment;
	}
	
	/*
	 * 分頁條件式查詢出貨單
	 */
	@SuppressWarnings("unchecked")
	public List<Shipment> queryShipmentByPage(
			Shipment shipment, String startTime, String endTime, int pageIndex, int pageSize) {
		Transaction transaction = null;
		Criteria criteria = null;
		List<Shipment> shipments = new ArrayList<>();
		Session session = sessionFactory.getCurrentSession();
		
		try {
			transaction = session.beginTransaction();
			criteria = createCriteriaAndSetCondition(
					session, shipment, startTime, endTime, pageIndex, pageSize);
			shipments = (List<Shipment>)criteria.list();
			transaction.commit();
		} catch(Exception e) {
			transaction.rollback();
			log.error("Exception : ", e);
		}
		
		return shipments;
	}
	
	/*
	 * 建立篩選條件
	 */
	private Criteria createCriteriaAndSetCondition(
			Session session, Shipment shipment, String startTime, String endTime, 
			int pageIndex, int pageSize) {
		String shipmentId = shipment.getShipmentId();
		String shipmentName = shipment.getShipmentName();
		String status = shipment.getStatus();
		Date startDate = DateUtil.parse("yyyy/MM/dd", startTime);
		Date endDate = DateUtil.parse("yyyy/MM/dd", endTime);
		Criteria criteria = session.createCriteria(Shipment.class);
		
		if(!StringUtil.isNullOrEmpty(shipmentId)) criteria.add(Restrictions.eq("shipmentId", shipmentId));
		if(!StringUtil.isNullOrEmpty(shipmentName)) 
			criteria.add(Restrictions.eq("shipmentName", shipmentName));
		if(!StringUtil.isNullOrEmpty(status) && Integer.parseInt(status) > 0) 
			criteria.add(Restrictions.eq("status", status));
		if((startDate != null) && (endDate != null)) 
			criteria.add(Restrictions.between("shipmentCreateTime", startDate, endDate));
		
		criteria.addOrder(Order.asc("shipmentId"));
		criteria.setFirstResult(pageIndex * pageSize);
		criteria.setMaxResults(pageSize);
		
		return criteria;
	}
	
	/*
	 * 下方為Shipment CRUD
	 */
	public Shipment getShipmentById(String shipmentId) {
		Shipment shipment = null;
		Transaction transaction = null;
		Session session = sessionFactory.getCurrentSession();
		
		try {
			transaction = session.beginTransaction();
			shipment = (Shipment)session
					.createCriteria(Shipment.class)
					.add(Restrictions.eq("shipmentId", shipmentId))
					.uniqueResult();
			transaction.commit();
		} catch(Exception e) {
			transaction.rollback();
			log.error("Exception : ", e);
		}
		
		return shipment;
	}
	
	public void saveShipment(Shipment shipment) {
		super.save(shipment);
	}
	
	public void updateShipment(Shipment shipment) {
		super.update(shipment);
	}
	
	public void deleteShipment(Shipment shipment) {
		super.delete(shipment);
	}
}
