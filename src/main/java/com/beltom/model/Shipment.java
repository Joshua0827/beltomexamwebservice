package com.beltom.model;

import static com.beltom.util.StringUtil.nl;

import java.io.Serializable;
import java.util.Date;
import java.util.Hashtable;
import java.util.Map;

public class Shipment implements Serializable {
	private static final long serialVersionUID = 8147092416142682669L;

	private String shipmentId;
	private String shipmentName;
	private String receiver;
	private String place;
	private String email;
	private String phone;
	private String status;
	private String stateString;
	private String remark;
	private String operator;
	
	private Date shipmentCreateTime;
	private Date planShipmentTime;
	private Date startShipmentTime;
	private Date finishShipmentTime;
	
	private static Map<String, String> state = null;
	
	static {
		state = new Hashtable<>();
		
		state.put("0", "草稿");
		state.put("1", "暫停");
		state.put("2", "可出貨");
		state.put("3", "出貨中");
		state.put("4", "出貨完成");
		state.put("5", "已取消");
	}
	
	public String getShipmentId() {
		return shipmentId;
	}
	
	public void setShipmentId(String shipmentId) {
		this.shipmentId = shipmentId;
	}
	
	public String getShipmentName() {
		return shipmentName;
	}

	public void setShipmentName(String shipmentName) {
		this.shipmentName = shipmentName;
	}

	public String getReceiver() {
		return receiver;
	}

	public void setReceiver(String receiver) {
		this.receiver = receiver;
	}

	public String getPlace() {
		return place;
	}

	public void setPlace(String place) {
		this.place = place;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	public String getStateString() {
		stateString = state.get(status);
		
		return stateString;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getOperator() {
		return operator;
	}

	public void setOperator(String operator) {
		this.operator = operator;
	}

	public Date getShipmentCreateTime() {
		return shipmentCreateTime;
	}

	public void setShipmentCreateTime(Date shipmentCreateTime) {
		this.shipmentCreateTime = shipmentCreateTime;
	}

	public Date getPlanShipmentTime() {
		return planShipmentTime;
	}

	public void setPlanShipmentTime(Date planShipmentTime) {
		this.planShipmentTime = planShipmentTime;
	}

	public Date getStartShipmentTime() {
		return startShipmentTime;
	}

	public void setStartShipmentTime(Date startShipmentTime) {
		this.startShipmentTime = startShipmentTime;
	}

	public Date getFinishShipmentTime() {
		return finishShipmentTime;
	}

	public void setFinishShipmentTime(Date finishShipmentTime) {
		this.finishShipmentTime = finishShipmentTime;
	}
	
	public String toString() {
		StringBuilder detail = new StringBuilder();
		
		detail.append("shipmentId:").append(shipmentId).append(nl);
		detail.append("shipmentName:").append(shipmentName).append(nl);
		detail.append("status:").append(status).append(nl);
		detail.append("receiver:").append(receiver).append(nl);
		detail.append("place:").append(place).append(nl);
		detail.append("phone:").append(phone).append(nl);
		detail.append("operator:").append(operator).append(nl);
		detail.append("shipmentCreateTime:").append(shipmentCreateTime).append(nl);
		detail.append("planShipmentTime:").append(planShipmentTime).append(nl);
		detail.append("startShipmentTime:").append(startShipmentTime).append(nl);
		detail.append("finishShipmentTime:").append(finishShipmentTime);

		return detail.toString();
	}
}
