package com.beltom.biz;

import java.util.Date;
import java.util.List;

import com.beltom.dao.ShipmentDao;
import com.beltom.model.Shipment;
import com.beltom.util.DateUtil;

/*
 * ShipmentDao操作相關的商業邏輯
 */
public class ShipmentDaoManager {
	private ShipmentDao dao = new ShipmentDao();
	
	/*
	 * 建立出貨單
	 */
	public void createShipment(Shipment shipment) {
		shipment.setShipmentId(getMaxShipmentId());
		dao.saveShipment(shipment);
	}
	
	/*
	 * 先取得資料庫最高單號，回傳產生最高的出貨單號
	 */
	private String getMaxShipmentId() {
		String header = "BEL" + DateUtil.format("yyyyMMddHHmmss", new Date());
		Shipment shipment = dao.getMaxShipmentByHeader(header);
		int serialNumber = 1;
		
		if(shipment != null) {
			int startPosition = shipment.getShipmentId().length() - 5;
			String serial = shipment.getShipmentId().substring(startPosition);
			serialNumber += Integer.parseInt(serial);
		}
		
		return header + String.format("%05d", serialNumber);
	}
	
	/*
	 * 取得符合條件查詢的紀錄幒筆數
	 */
	public int getShipmentCount(Shipment shipment, String startTime, String endTime) {
		//分頁查詢條件利用pageIndex與pageSize實現，故給予(0, 0)表示不限筆數
		return queryShipmentByPage(shipment, startTime, endTime, 0, 0).size();
	}
	
	/*
	 * 分頁條件式查詢出貨單
	 */
	public List<Shipment> queryShipmentByPage(
			Shipment shipment, String startTime, String endTime, int pageIndex, int pageSize) {
		return dao.queryShipmentByPage(shipment, startTime, endTime, pageIndex, pageSize);
	}
	
	/*
	 * 依出貨單號取得出貨單資料
	 */
	public Shipment getShipmentById(String shipmentId) {
		return (Shipment)dao.getShipmentById(shipmentId);
	}
	
	/*
	 * 修改出貨單資料
	 */
	public void updateShipment(Shipment shipment) {
		dao.updateShipment(shipment);
	}
	
	/*
	 * 刪除出貨單
	 */
	public void deleteShipment(Shipment shipment) {
		dao.deleteShipment(shipment);
	}
}
