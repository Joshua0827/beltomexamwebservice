package com.beltom.biz;

import com.beltom.dao.UserDao;
import com.beltom.model.User;

/*
 * UserDao操作相關的商業邏輯
 */
public class UserDaoManager {
	private UserDao dao = new UserDao();
	
	/*
	 * 驗證使用者名稱密碼
	 */
	public boolean isValidUser(User user) {
		User dbUser = dao.getUserByName(user.getUserName());
		
		return (dbUser != null) && (dbUser.getUserPassword().equals(user.getUserPassword()));
	}
	
	/*
	 * 依使用者名稱，檢查使用者是否已存在
	 */
	public boolean isUserExists(String userName) {
		return dao.getUserByName(userName) != null;
	}
	
	/*
	 * 註冊使用者
	 */
	public void createUser(User user) {
		user.setCreateDate(new java.util.Date());
		
		dao.saveUser(user);
	}
	
	/*
	 * 依id取得使用者資料
	 */
	public User obtainUserById(Integer id) {
		return dao.getUserById(id);
	}
	
	/*
	 * 修改使用者資料(密碼，信箱)
	 */
	public void updateUserData(User user) {		
		dao.updateUser(user);
	}
	
	/*
	 * 刪除使用者
	 */
	public void deleteUser(User user) {
		dao.deleteUser(user);
	}
}
