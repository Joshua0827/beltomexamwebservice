package com.beltom.util;

import java.util.regex.Pattern;

public class StringUtil {
	/*
	 * New Line
	 */
	public static final String nl = System.lineSeparator();
	
	public static final boolean isNullOrEmpty(String obj) {
		return (obj == null) || obj.isEmpty(); 
	}
	
	public static final boolean validateDateFormat(String dateString) {
		String regex = "^\\d{4}/(0?[1-9]|1[012])/(0?[1-9]|[12][0-9]|3[01])$";
		Pattern p = Pattern.compile(regex);
		
		return p.matcher(dateString).find();
	}
}
