package com.beltom.axis;

import java.util.Date;

import com.beltom.biz.ShipmentDaoManager;
import com.beltom.dao.ShipmentHistoryDao;
import com.beltom.model.Shipment;
import com.beltom.model.ShipmentHistory;

public class ShipmentCancelService {
	public String cancelShipment(String shipmentId) {
		ShipmentDaoManager manager = new ShipmentDaoManager();
		Shipment shipment = manager.getShipmentById(shipmentId);
		shipment.setStatus("5");
		manager.updateShipment(shipment);
		saveShipmentHistory(shipment);
		
		return "success";
	}
	
	private void saveShipmentHistory(Shipment shipment) {
		ShipmentHistory shipmentHistory = new ShipmentHistory();
		shipmentHistory.setShipmentId(shipment.getShipmentId());
		shipmentHistory.setStatus(shipment.getStatus());
		shipmentHistory.setCreateDate(new Date());
		
		ShipmentHistoryDao dao = new ShipmentHistoryDao();
		dao.saveShipmentHistory(shipmentHistory);
	}
}
